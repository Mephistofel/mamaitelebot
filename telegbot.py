#! /usr/bin/python3

import signal
import sys
import os
import requests
import tornado.web
import tornado.ioloop

BOT_TOKEN = "522432273:AAEl-j5ySfiEIkuTq0dacaBYazgVT1iLVtM"
URL = "https://api.telegram.org/bot%s/" % BOT_TOKEN
heroku_url = "https://mamaitelebot.herokuapp.com"


def send_response(text, chat_id):
    print("send to %s" % URL + "sendMessage")
    api.post(URL + "sendMessage?chat_id=%s&text=%s" %(chat_id, text))


class ReverseMagic(tornado.web.RequestHandler):

    def get(self):
        print("Hello!")

    def post(self):
        try:
            print("start post\n%s" % self.request.body)
            data = tornado.escape.json_decode(self.request.body)
            text = data["message"].get("text")
            chat_id = data["message"]["chat"].get("id")
            if text and chat_id:
                print("%s \t %s" % (chat_id, text))

                if text[0] != "/":
                    send_response(text[::-1], chat_id)
                # if text = "/start":
                #     send_response("Здравствуйте! Я MedAboutMeBot. Вы можете обращаться ко мне с любыми вопросами о здоровье!", chat_id)
                # if text[0] != "/":
                #     if text.find("?"):
                #         send_response("Воспользуйтесь нашим сервисом \"Задать вопрос врачу\". Наши специалисты помогут Вам в кратчайшие сроки\nhttps://medaboutme.ru/zdorove/servisy/zadat-vopros-vrachu/", chat_id)
                #     elif text.find("анализ"):
                #         send_response("Воспользуйтесь нашим сервисом \"Расшифровка анализов\".\nhttps://medaboutme.ru/zdorove/servisy/rasshifrovka-analizov/", chat_id)
                #     elif text.find("боль ") or text.find("боль.") or text.find(" боль") or text.find("болит") or text.find("болела"):
                #         send_response("Возможно Вам лучше сразу обратиться к врачу? Воспользуйтесь услугой на нашем сайте:\nhttps://medaboutme.ru/zdorove/servisy/rasshifrovka-analizov/", chat_id)
                #     else:
                #         send_response("Следующие материалы на нашем портале могут быть Вам интересны:", chat_id)
                #         send_response("https://medaboutme.ru/zdorove/publikacii/stati/sovety_vracha/7_zapakhov_tela_govoryashchikh_o_boleznyakh/", chat_id)
                #         send_response("https://medaboutme.ru/obraz-zhizni/publikacii/stati/pohudenie/kak_dobitsya_pokhudeniya_v_rayone_zhivota/", chat_id)
                #         send_response("https://medaboutme.ru/obraz-zhizni/publikacii/stati/profilaktika/7_vrednykh_produktov_vyzyvayushchikh_starenie_organizma/", chat_id)

        except Exception as e:
            print(e)


api = requests.Session()
application = tornado.web.Application([
    (r"/", ReverseMagic),
])


if __name__ == "__main__":

    print("Start")
    print(URL + "setWebhook?url=%s" % heroku_url)
    hook = api.get(URL + "setWebhook?url=%s" % heroku_url)

    if hook.status_code != 200:
        print("Error on set webhook")
        sys.exit(1)

    port = int(os.environ.get("PORT", 5000))
    application.listen(port)
    tornado.ioloop.IOLoop.current().start()
